import getpass
import threading
import requests
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

discogs_url = 'https://api.discogs.com'
currency = 'EUR'

vol1_id = 12207025
vol2_id = 12206991
singel_id = 12183479

password = getpass.getpass()

def send_mail(sales):
    print('sending mail')
    me = 'public@peddn.net'
    you = 'public@peddn.net'
    subject = 'Hol sie dir!'
    message = """
    Folgendes Saber Rider Vinyl wäre abgreifbar:
    Call Me Single: {}
    OST Vol. 1    : {}
    OST Vol. 2    : {}
    """.format(sales['single'], sales['vol1'], sales['vol2'])

    msg = MIMEMultipart()
    msg['From'] = me
    msg['To'] = you
    msg['Subject'] = subject

    msg.attach(MIMEText(message, 'plain'))

    server = smtplib.SMTP_SSL('smtp.peddn.net', 465)
    server.login(me, password)
    text = msg.as_string()
    server.sendmail(me, you, text)
    server.quit()

def check_for_sales():

    print('Check for sales...')

    sales = {}

    r = requests.get('{}/releases/{}?{}'.format(discogs_url, vol1_id, currency))
    sales['vol1'] = r.json()['num_for_sale']

    r = requests.get('{}/releases/{}?{}'.format(discogs_url, vol2_id, currency))
    sales['vol2'] = r.json()['num_for_sale']

    r = requests.get('{}/releases/{}?{}'.format(discogs_url, singel_id, currency))
    sales['single'] = r.json()['num_for_sale']

    if sales['vol1'] > 0 or sales['vol2'] > 0 or sales['single'] > 0:
        send_mail(sales)
    
    threading.Timer(10*60, check_for_sales).start()

check_for_sales()